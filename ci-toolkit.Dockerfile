ARG DOCKER_REGISTRY
FROM ${DOCKER_REGISTRY}/docker:19.03.12

RUN apk update && apk add --no-cache \
    curl \
    docker-compose \
    make
